import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys

tz = pytz.timezone('Asia/Bangkok')
path='/home/admin/Dropbox/Gen 1 Data/'
mailpath='/home/admin/CODE/Send_mail/mail_recipients.csv'

def sendmail(station,type,data,rec):
    SERVER = "smtp.office365.com"
    FROM = 'operations@cleantechsolar.com'
    recipients = rec # must be a list
    TO=", ".join(recipients)
    SUBJECT = station+' Comms Issue/Power Trip Alarm'
    text2='Last Timestamp Read: '+str(data) +'\n\n'
    TEXT = text2
    # Prepare actual message
    message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
    # Send the mail
    import smtplib
    server = smtplib.SMTP(SERVER)
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    server.sendmail(FROM, recipients, message)
    server.quit()

def rectolist(station,path):
    df=pd.read_csv(path)
    df2=df[['Recipients',station]]
    a=df2.loc[df2[station] == 1]['Recipients'].tolist()
    return a

starttime=time.time()
while(1):
    date_now=str(datetime.datetime.now(tz).date())
    print(date_now)
    stations=[]
    for i in sorted(os.listdir(path)):
        if 'MY-4' in i:
            if('MY-403' not in i):
                stations.append(i)
    print(stations)
    for i in stations:
        if(os.path.exists(path+'/'+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV Meter/'+i+'-MFM1-'+date_now+'.txt')):
            df=pd.read_csv(path+'/'+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/MFM_1_PV Meter/'+i+'-MFM1-'+date_now+'.txt',sep='\t')
        else:
            print('File not present!')
            time.sleep(3600)
            continue
        active_power=df[['ts','W_avg']].tail(12)
        print(active_power)
        active_power['ts'] =  pd.to_datetime(active_power['ts'])
        for index, row in active_power.iterrows():
            if(row['ts'].hour>8 and row['ts'].hour<18):
                if(active_power['W_avg'].sum()<1):
                    print(row['ts'])
                    print('Sending Mail!')
                    recipients=rectolist(i[1:-1]+'_Mail',mailpath)
                    sendmail(i,'Comms/Power Issue',row['ts'],recipients)
                    break
    print('Sleeping!')
    time.sleep(3600.0 - ((time.time() - starttime) % 3600.0))