import pandas as pd
import os
import numpy as np
import pandas as pd
import datetime
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats
import matplotlib.dates as mdates
from matplotlib.ticker import MaxNLocator
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.patches as mpatches
import pyodbc
import sys
from bisect import bisect
from matplotlib.text import Annotation
from matplotlib.transforms import Affine2D
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

class LineAnnotation(Annotation):
    def __init__(
        self, text, line, x , line_type, xytext=(0, 5), textcoords="offset points", **kwargs
    ):
        assert textcoords.startswith(
            "offset "
        ), "*textcoords* must be 'offset points' or 'offset pixels'"

        self.line = line
        self.xytext = xytext

        # Determine points of line immediately to the left and right of x
        xs, ys = line.get_data()

        assert (
            np.diff(xs) >= 0
        ).all(), "*line* must be a graph with datapoints in increasing x order"

        i = np.clip(bisect(xs, x), 1, len(xs) + 1)
        self.neighbours = n1, n2 = np.asarray([(xs[i - 1], ys[i - 1]), (xs[i], ys[i])])

        # Calculate y by interpolating neighbouring points
        if(line_type == 'below'):
            y = n1[1] + ((x - n1[0]) * (n2[1] - n1[1]) / (n2[0] - n1[0]))-3
        else:
            y = n1[1] + ((x - n1[0]) * (n2[1] - n1[1]) / (n2[0] - n1[0]))

        kwargs = {
            "horizontalalignment": "center",
            "rotation_mode": "anchor",
            **kwargs,
        }
        super().__init__(text, (x, y), xytext=xytext, textcoords=textcoords, **kwargs)

    def get_rotation(self):

        transData = self.line.get_transform()
        dx, dy = np.diff(transData.transform(self.neighbours), axis=0).squeeze()
        return np.rad2deg(np.arctan2(dy, dx))

    def update_positions(self, renderer):
        xytext = Affine2D().rotate_deg(self.get_rotation()).transform(self.xytext)
        self.set_position(xytext)
        super().update_positions(renderer)


def line_annotate(text, line, x , line_type, *args, **kwargs):
    ax = line.axes
    a = LineAnnotation(text, line, x, line_type, *args, **kwargs)
    if "clip_on" in kwargs:
        a.set_clip_path(ax.patch)
    ax.add_artist(a)
    return a

def get_total_eac(date):
    connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    SQL_Query = pd.read_sql_query("SELECT SUM([Eac-MFM]) AS SUM FROM [dbo].[Stations_Data] WHERE Station_Id=(SELECT Station_Id FROM [dbo].[Stations] WHERE Station_Name='IN-069') AND Date='"+date+"' ", connStr)
    df_sum = pd.DataFrame(SQL_Query, columns=['SUM'])
    return df_sum
    
#Sending Message
def send_mail(date, stn, df, recipients, attachment_path_list=None):
  info ='Date: '+date+'\n\n'
  info = info+"GHI : "+str(df[0]['GHI'].values[0])+'\n\n'
  info = info+"PR : "+str(round(df[0]['PR'].values[0],1))+'\n\n'
  server = smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg = MIMEMultipart()
  sender = 'operations@cleantechsolar.com'
  msg['Subject'] = stn 
  today_g = df_today['GHI'].values[0]
  today_pr = df_today['PR'].values[0]
  if('Upper' in stn):
      info = info+'Alarm type: Upper Corridor\n\n'
      trig_val = round((((today_pr-(slope*today_g+(intercept)))/today_pr))*100,1)
  elif('Lower' in stn):
      info = info+'Alarm type: Lower Corridor\n\n'
      temp_trig = (slope*today_g+(intercept))
      trig_val = round(((temp_trig - today_pr)/temp_trig)*100,1)
  #if sender is not None:
  msg['From'] = sender
  msg['To'] = ", ".join(recipients)
  info = info + 'Alarm Trigger Value: '+str(trig_val)+' %\n\n'
  if attachment_path_list is not None:
    for each_file_path in attachment_path_list:
      try:
        file_name = each_file_path.split("/")[-1]
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(each_file_path, "rb").read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
        msg.attach(part)
      except:
        print("could not attache file")
  msg.attach(MIMEText(info,'plain/text'))
  server.sendmail(sender, recipients, msg.as_string())
  
def abline(slope, intercept, df, df_today):
    x_vals = np.array(ax.get_xlim())
    y_vals = intercept + slope * x_vals
    y_vals2 = (intercept+0.05*intercept) + slope * x_vals
    y_vals3 = (intercept-0.05*intercept) + slope * x_vals
    g = df['GHI'].tolist()
    p = df['PR'].tolist()
    cnt=0
    for index,i in enumerate(g):
        temp = slope*i+(intercept+0.05*intercept)
        temp2 = slope*i+(intercept-0.05*intercept)
        if(p[index]>temp or p[index]<temp2):
            cnt=cnt+1
    total = len(df)
    ax.annotate('Inside corridor = '+str(total-cnt)+'/'+str(total)+' = '+str(round(((total-cnt)/total)*100,1))+'%', (.99, .05),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='right', va='bottom',size=11)
    plt.plot(x_vals, y_vals,color='blue')
    line_1 = plt.plot(x_vals, y_vals2,'--',color='blue')
    line_2 = plt.plot(x_vals, y_vals3,'--',color='blue')
    line_annotate('+5% variation from mean',line_1[0],4,'above',color='blue')
    line_annotate('-5% variation from mean',line_2[0],3.8,'below',color='blue')
"""     if((today_pr>slope*today_g+(intercept+0.05*intercept))):
        send_mail(df_today['Date'].values[0],'Upper IN-069 PR Corridor Alarm',[df_today,slope,intercept],['operationsSG@cleantechsolar.com','sai.pranav@cleantechsolar.com'])
    elif((today_pr<slope*today_g+(intercept-0.05*intercept))):
        send_mail(df_today['Date'].values[0],'Lower IN-069 PR Corridor Alarm',[df_today,slope,intercept],['operationsSG@cleantechsolar.com','sai.pranav@cleantechsolar.com'])
 """

    
font = {'size'   : 11}

matplotlib.rc('font', **font)


date_today = sys.argv[1]


path='/home/admin/Graphs/Graph_Extract/IN-069/[IN-069] Graph '+date_today+' - PR Evolution.txt'

df=pd.read_csv(path,sep='\t')

df=df[df['Date']>='2020-01-01']
df_temp = df.copy()

df_today = df[df['Date']==date_today]

df = df[(df['Date']!=date_today)]

fig3 = plt.figure(num=None, figsize=(13.2  , 8.8))

ax = fig3.add_subplot(111)

shape_dict = {1:'s',2:'s',3:'s',4:'s',5:'s',6:'v',7:'v',8:'v',9:'v',10:'s',11:'s',12:'s'}
temp_dict = {}

dates = df['Date'].tolist()
g_vals = df['GHI'].tolist()
pr_vals = df['PR'].tolist()


for index,i in enumerate(pr_vals):
    month = datetime.datetime.strptime(dates[index], '%Y-%m-%d').month

    if(g_vals[index]<2):
        c='darkblue'
    elif(g_vals[index]>=2 and g_vals[index]<=4):
        c='deepskyblue'
    elif(g_vals[index]>=4 and g_vals[index]<=6):
        c='orange'
    elif(g_vals[index]>6):
        c='chocolate'
    if(month == 1 or month==2 or month==3 or month==4 or month==5 or month==10 or month==11 or month==12):
        ln14=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict['Dry'] = ln14
    else:
        ln13=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict['Wet'] = ln13
   

today_g = df_today['GHI'].values[0]
today_pr = df_today['PR'].values[0]
ln5 = ax.scatter(today_g, today_pr, marker='*',color='black',s=60, alpha=0.75, zorder=3)
temp_dict[date_today] = ln5

g_patch = mpatches.Patch(color='white', label='GHI [kWh/m2]')
kh_patch = mpatches.Patch(color='darkblue', label='< 2')
th_patch = mpatches.Patch(color='deepskyblue', label='2 ~ 4')
vn_patch = mpatches.Patch(color='orange', label='4 ~ 6')
ph_patch = mpatches.Patch(color='chocolate', label='> 6')

df_wet = df[(df['Date']>='2020-06-01') & (df['Date']<'2020-10-01')]
X = df_wet['GHI'].values.reshape(-1, 1)
Y = df_wet['PR'].values.reshape(-1, 1)
linear_regressor = LinearRegression()  # create object for the class
linear_regressor.fit(X, Y)  # perform linear regression
slope=linear_regressor.coef_[0][0]
intercept=linear_regressor.intercept_[0]
legend = plt.legend(list(temp_dict.values()),list(temp_dict.keys())+[date_today],scatterpoints=1,fontsize=11,loc=(0.05,.05),frameon=False)
totlen=len(list(temp_dict.values()))
for m in range(totlen):
    legend.legendHandles[m].set_color('black')

plt.gca().add_artist(legend)
plt.legend(handles=[g_patch,kh_patch,th_patch,vn_patch,ph_patch],loc=(0.38,.92),frameon=False,ncol=5,fontsize=11)
x=[today_g]
y=[today_pr]
plt.ylim(0,100)
plt.xlim(0,7.5)
plt.vlines(x, 0, y, linestyle="dashed",color='red')
plt.hlines(y, 0, x, linestyle="dashed",color='red' )
plt.text(x[0]-.15,10, str(x[0])+ ' [kWh/m2]', ha='left', va='center', rotation=90,color='red')
plt.text(0.05,y[0]-2, str(round(y[0],1))+' [%]', ha='left', va='center',color='red')
df_sum = get_total_eac(date_today)
total_eac = (df_sum['SUM'].values[0])/1000
plt.text(x[0]/2,y[0]/2, 'EAC = '+str(round(total_eac,1))+' MWh', ha='left', va='center',color='red')
abline(slope,intercept,df_temp,df_today)

#ax3.plot(df['GHI'],Y_pred,color='black',lw=2,ls='-',label='Linear Fit')
ax.set_ylabel("PR [%]")
ax.set_xlabel("GHI [kWh/m2]")
ttl = ax.set_title('From '+str(df['Date'].head(1).values[0])[0:10]+' to '+str(df_temp['Date'].tail(1).values[0])[0:10], fontdict={'fontsize': 13, 'fontweight': 'medium'})
ttl.set_position([.495, 1.02])
ttl_main=fig3.suptitle('IN-069 GHI vs PR',fontsize=15,x=.512)

ax.annotate('y='+str(round(slope,2))+'x+'+str(round(intercept,2)), (.99, .1),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='right', va='bottom',size=11)

fig3.savefig("/home/admin/Graphs/Graph_Output/IN-069/[IN-069] Graph "+date_today+" - GHI vs PR.pdf", bbox_inches='tight')
df_temp.to_csv("/home/admin/Graphs/Graph_Extract/IN-069/[IN-069] Graph "+date_today+" - GHI vs PR.txt",sep='\t')


