source('/home/admin/CODE/common/math.R')
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}

prepareSummFab35 = function(date,wait)
{
  yr = substr(date,1,4)
  mon = substr(date,1,7)
  pathMain = paste("/home/admin/Dropbox/Second Gen/[SG-725S]/",yr,"/",mon,"/[SG-725S] ",date,".txt",sep="")
  colsIntreset = c(4,5,6,7,9,10,11,12,15,16,13)
  vals = unlist(rep(0,length(colsIntreset)))
  vals[7] = ""
  {
    if(!file.exists(pathMain) && wait)
    {
      print(paste(pathMain,"Doesnt exist sleeping 2hrs"))
      Sys.sleep(7200) # Wait two hours for the file.. good odds rest of the files will land
      return(prepareSummFab35(date,0))
    }
    else if(file.exists(pathMain))
    {
      dataread = read.table(pathMain,header=T,sep="\t",stringsAsFactors=F)
      vals = dataread[1,colsIntreset]
    }
  }
  return(vals)
}

prepareSummFab7 = function(date,wait)
{
  yr = substr(date,1,4)
  mon = substr(date,1,7)
  pathMain = paste("/home/admin/Dropbox/Second Gen/[SG-726S]/",yr,"/",mon,"/[SG-726S] ",date,".txt",sep="")
  colsIntreset = c(4,5,6,7,9,10,11,12,14,15,16,17,18,19,20,21,23,24,25,26,27,28,29,30,31,32,33,34,13,22)
  vals = unlist(rep(0,length(colsIntreset)))
  vals[7] = ""
  vals[15] = ""
  {
    if(!file.exists(pathMain) && wait)
    {
      print(paste(pathMain,"Doesnt exist sleeping 2hrs"))
      Sys.sleep(7200) # Wait two hours for the file.. good odds rest of the files will land
      return(prepareSummFab7(date,0))
    }
    else if(file.exists(pathMain))
    {
      dataread = read.table(pathMain,header=T,sep="\t",stringsAsFactors=F)
      vals = dataread[1,colsIntreset]
    }
  }
  print("Vals returned")
  print(vals)
  print("#################")
  return(vals)
}
prepareSummFab7G = function(date,wait)
{
  yr = substr(date,1,4)
  mon = substr(date,1,7)
  pathMain = paste("/home/admin/Dropbox/Second Gen/[SG-727S]/",yr,"/",mon,"/[SG-727S] ",date,".txt",sep="")
  colsIntreset = c(4,5,6,7,9,10,11,12,13,14,15,16)
  vals = unlist(rep(0,length(colsIntreset)))
  vals[7] = ""
  {
    if(!file.exists(pathMain) && wait)
    {
      print(paste(pathMain,"Doesnt exist sleeping 2hrs"))
      Sys.sleep(7200) # Wait two hours for the file.. good odds rest of the files will land
      return(prepareSummFab7G(date,0))
    }
    else if(file.exists(pathMain))
    {
      dataread = read.table(pathMain,header=T,sep="\t",stringsAsFactors=F)
      vals = dataread[1,colsIntreset]
    }
  }
  return(vals)
}

prepareSumm = function(dataread,wait)
{
  APPENDWARNING <<- 0
  da = nrow(dataread)
  daPerc = round(da/14.4,1)
  thresh = 5/1000
  gsi1=gsi2=tambsh=tambmxsh=tambmnsh=tamb=tambmx=tambmn=hamb=hambmx=hambmn=recfab2=NA
  hambsh=hambmxsh=hambmnsh=tmod=tmodsh=tmodmx=tmodmxsh=tmodmn=tmodmnsh=NA
  
  dataread2 = dataread[complete.cases(dataread[,3]),3]
  if(length(dataread2))
    gsi1 = sum(dataread[complete.cases(dataread[,3]),3])/60000
  dataread2 = dataread[complete.cases(dataread[,4]),4]
  if(length(dataread2))
    gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
  #  gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
  #  gsismp = sum(dataread[complete.cases(dataread[,5]),5])/60000
  subdata = dataread[complete.cases(dataread[,3]),]
  subdata = subdata[as.numeric(subdata[,3]) > thresh,]
  
  dataread2 = subdata[complete.cases(subdata[,6]),6]
  if(length(dataread2))
  {
    tambsh = mean(dataread2)
    tambmxsh = max(dataread2)
    tambmnsh = min(dataread2) 
  }
  dataread2 = subdata[complete.cases(subdata[,7]),7]
  if(length(dataread2))
  {
    hambsh = mean(dataread2)
    hambmxsh = max(dataread2)
    hambmnsh = min(dataread2)
  }
  dataread2 = subdata[complete.cases(subdata[,8]),8]
  if(length(dataread2))
  {
    tsish = mean(dataread2)
    tsimxsh = max(dataread2)
    tsimnsh = min(dataread2)
  }
  
  dataread2 = dataread[complete.cases(dataread[,6]),6]
  if(length(dataread2))
  {
    tamb = mean(dataread2)
    tambmx = max(dataread2)
    tambmn = min(dataread2) 
  }
  dataread2 = dataread[complete.cases(dataread[,7]),7]
  if(length(dataread2))
  {
    hamb = mean(dataread2)
    hambmx = max(dataread2)
    hambmn = min(dataread2)
  }
  dataread2 = dataread[complete.cases(dataread[,8]),8]
  if(length(dataread2))
  {
    tsi = mean(dataread2)
    tsimx = max(dataread2)
    tsimn = min(dataread2)
  }
  
  
  gsirat = gsi1 / gsi2
  #  smprat = gsismp / gsi2
  
  Eac11 = Eac21 =LastR1=LastT1=NA
  
  dataread2 = dataread[complete.cases(dataread[,24]),24]
  if(length(dataread2))
  {
    Eac11 = sum(dataread[complete.cases(dataread[,24]),24])/60
  }
  dataread2 = dataread[complete.cases(dataread[,39]),39]

  cond = ( dataread2 > 0 &  dataread2 < 1000000000)
	dataread2 =  dataread2[cond]
  if(length(dataread2))
  {
    Eac21 = as.numeric(dataread2[length(dataread2)])  - as.numeric(dataread2[1])
    LastR1 = dataread2[length(dataread2)]
    LastT1 = as.character(dataread[complete.cases(dataread[,39]),1])
    LastT1 = LastT1[length(LastT1)]
    recfab2 =  as.character(dataread[complete.cases(dataread[,40]),40])
    recfab2 = recfab2[length(recfab2)]
  }
  PR11 = (Eac11 * 100) / (gsi1*794.97)
  PR21 = (Eac21 * 100) / (gsi1*794.97)
  PR11si = (Eac11 * 100) / (gsi2*794.97)
  PR21si = (Eac21 * 100) / (gsi2*794.97)
  YLD11 = 0.01*PR11 * gsi1
  YLD21 = 0.01*PR21 * gsi1
  
  dateAc = NA
  if(nrow(dataread))
    dateAc = substr(dataread[1,1],1,10)
  
  fab35 = prepareSummFab35(dateAc,wait)
  fab7 = prepareSummFab7(dateAc,wait)
  fab7G = prepareSummFab7G(dateAc,wait)
  
  yrcur = as.numeric(substr(dateAc,1,4))
  moncur = as.numeric(substr(dateAc,6,7))
  daycurr = as.numeric(substr(dateAc,9,10))
  {
    if((yrcur >= 2020 && moncur >= 2) || (yrcur == 2020 && moncur == 1 && daycurr >= 20))
    {
      FullSiteProd1 = Eac11 + as.numeric(fab35[1]) + as.numeric(fab7[1]) + as.numeric(fab7[9]) + as.numeric(fab7G[1])
      FullSiteProd2 = Eac21 + as.numeric(fab35[2]) + as.numeric(fab7[2]) + as.numeric(fab7[10]) + as.numeric(fab7G[2])
      denomUse = 3553.57
    }
    else if(yrcur > 2019 || (yrcur == 2019 && moncur > 10) || (yrcur == 2019 && moncur == 10 && daycurr > 21))
    {
      FullSiteProd1 = Eac11 + as.numeric(fab35[1]) + as.numeric(fab7[1]) + as.numeric(fab7[9]) + as.numeric(fab7G[1])
      FullSiteProd2 = Eac21 + as.numeric(fab35[2]) + as.numeric(fab7[2]) + as.numeric(fab7[10]) + as.numeric(fab7G[2])
      denomUse = 3067.57
    }
    else
    {
      FullSiteProd1 = Eac11 + as.numeric(fab35[1])
      FullSiteProd2 = Eac21 + as.numeric(fab35[2])
      denomUse = 1837.77
    }
  }
  FullSiteYld1=(FullSiteProd1 / (denomUse))
  FullSiteYld2=(FullSiteProd2 / (denomUse))
  
  FullSitePR1=(FullSiteProd1*100 / (denomUse)/gsi1)
  FullSitePR2=(FullSiteProd2*100 / (denomUse)/gsi1)
  
  FullSitePRsi1=(FullSiteProd1*100 / (denomUse)/gsi2)
  FullSitePRsi2=(FullSiteProd2*100 / (denomUse)/gsi2)
  
  ylds1 = c(YLD21,as.numeric(fab35[3]))
  ylds2 = c(YLD21,as.numeric(fab35[4]))
  if(yrcur > 2019 || ((yrcur == 2019 && moncur > 10) || (yrcur == 2019 && moncur == 10 && daycurr > 21)))
  {
    ylds1 = c(YLD21,as.numeric(fab35[3]),as.numeric(fab7[3]), as.numeric(fab7[11]), as.numeric(fab7G[3]))
    ylds2 = c(YLD21,as.numeric(fab35[4]),as.numeric(fab7[4]),as.numeric(fab7[12]),as.numeric(fab7G[4]))
  }
  sddev1 = sdp(ylds1)
  sddev2 = sdp(ylds2)
  cov1 = sddev1 * 100 / mean(ylds1)
  cov2 = sddev2 * 100 / mean(ylds2)
  
  timestamps_irradiance_greater_20 = dataread[dataread[, 4]>20, 1]
  
  #Meter_F2
  timestamps_freq_greater_40 = dataread[(dataread[,37]>40 & dataread[,37]!= "NaN") ,1]
  timestamps_pow_greater_2 = dataread[abs(dataread[,24])>2,1]
  common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)
  common2 = intersect(common, timestamps_pow_greater_2)
  GAFab2 = round(((length(common)/length(timestamps_irradiance_greater_20))*100), 1)
  PAFab2 = round(((length(common2)/length(common))*100), 1)
  
  #Meter_Fab35
  GAFab35 = as.numeric(fab35[9])
  PAFab35 = as.numeric(fab35[10])
  
  #Meter_Fab71
  GAFab71 = as.numeric(fab7[23])
  PAFab71 = as.numeric(fab7[24])
  
  #Meter_Fab72
  GAFab72 = as.numeric(fab7[25])
  PAFab72 = as.numeric(fab7[26])
  
  #Meter_Fab7
  GAFab7 = as.numeric(fab7[27])
  PAFab7 = as.numeric(fab7[28]) 
  
  #Meter_Fab7G
  GAFab7G = as.numeric(fab7G[11])
  PAFab7G = as.numeric(fab7G[12])
  
  INSTCAPM = c(794.970, 1042.800, 819.000, 410.800, 486.000)
  GAFULL = round(((GAFab2 * INSTCAPM[1] + GAFab35 * INSTCAPM[2] + GAFab71 * INSTCAPM[3] + GAFab72 * INSTCAPM[4] +  GAFab7G * INSTCAPM[5])/sum(INSTCAPM)), 1)
  PAFULL = round(((PAFab2 * INSTCAPM[1] + PAFab35 * INSTCAPM[2] + PAFab71 * INSTCAPM[3] + PAFab72 * INSTCAPM[4] +  PAFab7G * INSTCAPM[5])/sum(INSTCAPM)), 1)
  
  
  datawrite = data.frame(Date = dateAc,
                         PtsRec = rf(da),
                         GPy = rf(gsi1), 
                         GSi = rf(gsi2), 
                         GRat = rf3(gsirat),
                         Tamb = rf1(tamb),
                         TambMx = rf1(tambmx),
                         TambMn = rf1(tambmn),
                         TambSH = rf1(tambsh),
                         TambMxSH = rf1(tambmxsh),
                         TambMnSH = rf1(tambmnsh),
                         Hamb = rf1(hamb),
                         HambMx = rf1(hambmx),
                         HambMn = rf1(hambmn),
                         HambSH = rf1(hambsh),
                         HambMxSH = rf1(hambmxsh),
                         HambMnSH = rf1(hambmnsh),
                         TSi = rf1(tamb),
                         TSiMx = rf1(tambmx),
                         TSiMn = rf1(tambmn),
                         TSiSH = rf1(tambsh),
                         TSiMxSH = rf1(tambmxsh),
                         TSiMnSH = rf1(tambmnsh),
                         SDYld1 = rf3(sddev1),
                         SDYld2 = rf3(sddev2),
                         CovYld1 = rf1(cov1),
                         CovYld2 = rf1(cov2),
                         FullSite1 = rf(FullSiteProd1),
                         FullSite2 = rf(FullSiteProd2),
                         FullSiteYld1 = rf(FullSiteYld1),
                         FullSiteYld2 = rf(FullSiteYld2),
                         FullSitePR1 = rf1(FullSitePR1),
                         FullSitePR2 = rf1(FullSitePR2),
                         FullSitePRSi1 = rf1(FullSitePRsi1),
                         FullSitePRSi2 = rf1(FullSitePRsi2),
                         Eac11 = rf(Eac11),
                         Eac21 = rf(Eac21),
                         Yld11 = rf(YLD11),
                         Yld21 = rf(YLD21),
                         PR11 = rf1(PR11),
                         PR21 = rf1(PR21),
                         PR11Si = rf1(PR11si),
                         PR21Si = rf1(PR21si),
                         LastR1 = LastR1,
                         LastT1 = LastT1,
                         DA = rf1(daPerc),
                         Eac1Fab35 = as.numeric(fab35[1]),
                         Eac2Fab35 = as.numeric(fab35[2]),
                         Yld1Fab35 = as.numeric(fab35[3]),
                         Yld2Fab35 = as.numeric(fab35[4]),
                         PR1Fab35 = as.numeric(fab35[5]),
                         PR2Fab35 = as.numeric(fab35[6]),
                         LastTimeFab35 = as.character(fab35[7]),
                         LastReadFab35 = as.numeric(fab35[8]),
                         Eac1Fab71 = as.numeric(fab7[1]),
                         Eac2Fab71 = as.numeric(fab7[2]),
                         Yld1Fab71 = as.numeric(fab7[3]),
                         Yld2Fab71 = as.numeric(fab7[4]),
                         PR1Fab71 = as.numeric(fab7[5]),
                         PR2Fab71 = as.numeric(fab7[6]),
                         LastTimeFab71 = as.character(fab7[7]),
                         LastReadFab71 = as.numeric(fab7[8]),
                         Eac1Fab72 = as.numeric(fab7[9]),
                         Eac2Fab72 = as.numeric(fab7[10]),
                         Yld1Fab72 = as.numeric(fab7[11]),
                         Yld2Fab72 = as.numeric(fab7[12]),
                         PR1Fab72 = as.numeric(fab7[13]),
                         PR2Fab72 = as.numeric(fab7[14]),
                         LastTimeFab72 = as.character(fab7[15]),
                         LastReadFab72 = as.numeric(fab7[16]),
                         Eac1Fab7Tot = as.numeric(fab7[17]),
                         Eac2Fab7Tot = as.numeric(fab7[18]),
                         Yld1Fab7Tot = as.numeric(fab7[19]),
                         Yld2Fab7Tot = as.numeric(fab7[20]),
                         PR1Fab7Tot = as.numeric(fab7[21]),
                         PR2Fab7Tot = as.numeric(fab7[22]),
                         Eac1Fab7G = as.character(fab7G[1]),
                         Eac2Fab7G = as.character(fab7G[2]),
                         Yld1Fab7G = as.character(fab7G[3]),
                         Yld2Fab7G = as.character(fab7G[4]),
                         PR1Fab7G = as.character(fab7G[5]),
                         PR2Fab7G = as.character(fab7G[6]),
                         LastTimeFab7G = as.character(fab7G[7]),
                         LastReadFab7G = as.character(fab7G[8]),
                         LastReadFab2Rec = recfab2,
                         LastReadFab35Rec = as.character(fab35[11]),
                         LastReadFab21Rec = as.character(fab7[29]),
                         LastReadFab22Rec = as.character(fab7[30]),
                         LastReadFab7GRec = as.character(fab7G[9]),
                         LastReadFab7GRecDel = as.character(fab7G[10]),
                         GAFab2 = GAFab2,
                         PAFab2 = PAFab2,
                         GAFab35 = GAFab35,
                         PAFab35 = PAFab35,
                         GAFab71 = GAFab71,
                         PAFab71 = PAFab71,
                         GAFab72 = GAFab72,
                         PAFab72 = PAFab72,
                         GAFab7 = GAFab7,
                         PAFab7 = PAFab7,
                         GAFab7G = GAFab7G,
                         PAFab7G = PAFab7G,
                         GAFULL = GAFULL,
                         PAFULL = PAFULL,
                         stringsAsFactors=F
  )
  datawrite
}

rewriteSumm = function(datawrite)
{
  df = datawrite
  df
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[724]"
pathwrite = "/home/admin/Dropbox/Second Gen/[SG-005S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in 1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[SG-005S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread,0)
      datasum = rewriteSumm(datawrite)
      currdayw = gsub("724","SG-005S",days[z])
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
