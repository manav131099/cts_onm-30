rm(list = ls(all = T))
source('/home/admin/CODE/common/math.R')
INSTCAP = 1042.80
DAYSACTIVE = 0
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}

fetchGSIData = function(date,wait)
{
	pathMain = '/home/admin/Dropbox/Second Gen/[SG-724S]'
	yr = substr(date,1,4)
	mon = substr(date,1,7)
	txtFileName = paste('[SG-724S] ',date,".txt",sep="")
	print(txtFileName)
	pathyr = paste(pathMain,yr,sep="/")
	gsiVal = NA
	if(file.exists(pathyr))
	{
		pathmon = paste(pathyr,mon,sep="/")
		if(file.exists(pathmon))
		{
			pathfile = paste(pathmon,txtFileName,sep="/")
			if(file.exists(pathfile))
			{
				dataread = read.table(pathfile,sep="\t",header = T)
				gsiVal = as.numeric(dataread[,3])
			}
		}
	}
	if(wait && (!is.finite(gsiVal)))
	{
		print("Couldnt find file, sleeping for an hour")
		Sys.sleep(3600)
		gsiVal = fetchGSIData(date,0)
	}
	return(gsiVal)
}

prepareSumm = function(dataread1, wait, dataread2=NA)
{
  da = nrow(dataread1)
  daPerc = round(da/14.4,1)
	thresh = 5
	LastRead = LastTime = Eac1 = Eac2 = Eac3 = Eac4 = GA = PA = NA
	
	date = substr(as.character(dataread1[1,1]),1,10)
	Eac1 = as.numeric(dataread1[,16])
	Eac1 = Eac1[complete.cases(Eac1)]
	
	{
	if(length(Eac1))
		Eac1 = sum(Eac1)/60
	else
		Eac1 = NA
	}

	Eac2 = as.numeric(dataread1[,31])
	time = as.character(dataread1[,1])
	time = time[complete.cases(Eac2)]
	Eac2 = Eac2[complete.cases(Eac2)]
	cond = (Eac2 > 0 & Eac2 < 1000000000)
	Eac2 = Eac2[cond]
	{
	if(length(Eac2))
	{
		LastRead = Eac2[length(Eac2)]
		Eac2 = (Eac2[length(Eac2)] - Eac2[1])
		LastTime = time[length(time)]
	}
	else
		Eac2 = NA
	}
	
	Eac3 = as.numeric(dataread1[,32])
	Eac3 = Eac3[complete.cases(Eac3)]
	{
		if(length(Eac3))
			Eac3 = Eac3[length(Eac3)]
		else
			Eac3 = NA
	}
	
	Eac4 = as.numeric(dataread1[,34])
	Eac4 = Eac4[complete.cases(Eac4)]
	{
		if(length(Eac4))
			Eac4 = Eac4[length(Eac4)]
		else
			Eac4 = NA
	}
  
	Yld1 = Eac1/INSTCAP
	Yld2 = Eac2/INSTCAP
	IrrSG724 = fetchGSIData(date,wait)
	PR1 = Yld1*100/IrrSG724
	PR2 = Yld2*100/IrrSG724
  
  if (length(dataread2) > 1){
  timestamps_irradiance_greater_20 = dataread2[dataread2[, 4]>20, 1]
  timestamps_freq_greater_40 = dataread1[(dataread1[,29]>40 & dataread1[,29]!= "NaN") ,1]
  timestamps_pow_greater_2 = dataread1[abs(dataread1[,16])>2,1]
  common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)
  common2 = intersect(common, timestamps_pow_greater_2)
  GA = round(((length(common)/length(timestamps_irradiance_greater_20))*100), 1)
  PA = round(((length(common2)/length(common))*100), 1)
  }
  
  datawrite = data.frame(Date = date, PtsRec = da, DA = rf1(daPerc), Eac1 = rf(Eac1), Eac2 = rf(Eac2), Yld1 = rf(Yld1), Yld2 = rf(Yld2), GSiSG724 = IrrSG724, 
  PR1 = rf1(PR1), PR2 = rf1(PR2), LastTime = LastTime, LastRead = LastRead, LastReadRec = Eac3, LastReadRecDel = Eac4, GA = GA, PA = PA, stringsAsFactors=F)
  
  datawrite 
}

rewriteSumm = function(datawrite)
{
  datawrite
}

path1 = "/home/admin/Dropbox/Cleantechsolar/1min/[725]"
path2 = "/home/admin/Dropbox/Cleantechsolar/1min/[724]"
pathwrite = "/home/admin/Dropbox/Second Gen/[SG-725S]"
checkdir(pathwrite)
years1 = dir(path1)
x=y=z=1
for(x in 1 : length(years1))
{
  pathyear1 = paste(path1,years1[x],sep="/")
  pathyear2 = paste(path2,years1[x],sep="/")
  writeyear = paste(pathwrite,years1[x],sep="/")
  checkdir(writeyear)
  months1 = dir(pathyear1)
  for(y in  1: length(months1))
  {
    pathmonth1 = paste(pathyear1,months1[y],sep="/")
    pathmonth2 = paste(pathyear2,months1[y],sep="/")
    writemonth = paste(writeyear,months1[y],sep="/")
    checkdir(writemonth)
    days1 = dir(pathmonth1)
    sumfilename = paste("[SG-725S] ",substr(months1[y],3,4),substr(months1[y],6,7),".txt",sep="")
    for(z in 1 : length(days1))
    {
      dataread1 = read.table(paste(pathmonth1,days1[z],sep="/"),sep="\t",header = T)
      fname = paste("[724]", substr(days1[z],7,20),sep=" ")
      
      if(file.exists(paste(pathmonth2,fname,sep="/")))
        dataread2 = read.table(paste(pathmonth2,fname,sep="/"),sep="\t",header = T)
      else
        dataread2 = NA
                
      datawrite = prepareSumm(dataread1, 0, dataread2) #dont wait for history
      datasum = rewriteSumm(datawrite)
			currdayw = gsub("725","SG-725S",days1[z])
			if(x == 1 && y == 1 && z == 1)
				DOB = substr(days1[z],10,19)
			DAYSACTIVE = DAYSACTIVE + 1
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
