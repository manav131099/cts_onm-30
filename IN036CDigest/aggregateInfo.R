source('/home/admin/CODE/common/aggregate.R')

METERACNAMESAGG = c("WMS","CI_MFM","SI_MFM","C_Inverter_1","C_Inverter_2","S_Inverter_1",
"S_Inverter_2","S_Inverter_3","S_Inverter_4","S_Inverter_5","S_Inverter_6",
"S_Inverter_7","S_Inverter_8","S_Inverter_9","S_Inverter_10","S_Inverter_11",
"S_Inverter_12","S_Inverter_13","S_Inverter_14","S_Inverter_15","S_Inverter_16",
"S_Inverter_17","S_Inverter_18","S_Inverter_19","S_Inverter_20","S_Inverter_21",
"S_Inverter_22","S_Inverter_23","S_Inverter_24","S_Inverter_25","S_Inverter_26",
"S_Inverter_27","S_Inverter_28","S_Inverter_29","S_Inverter_30","S_Inverter_31",
"S_Inverter_32","S_Inverter_33","S_Inverter_34","S_Inverter_35","S_Inverter_36",
"S_Inverter_37","S_Inverter_38","S_Inverter_39","S_Inverter_40","S_Inverter_41",
"S_Inverter_42","S_Inverter_43","S_Inverter_44","S_Inverter_45","S_Inverter_46","S_Inverter_47")

registerMeterList("IN-036C",METERACNAMESAGG)
for( x in 1 : length(METERACNAMESAGG))
{
		aggNameTemplate = getNameTemplate()
		aggColTemplate = getColumnTemplate()
		{
		if(grepl("WMS",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = NA #column for LastRead
			aggColTemplate[4] = NA #column for LastTime
			aggColTemplate[5] = NA #column for Eac-1
			aggColTemplate[6] = NA #column for Eac-2
			aggColTemplate[7] = NA #column for Yld-1
			aggColTemplate[8] = NA #column for Yld-2
			aggColTemplate[9] = NA #column for PR-1
			aggColTemplate[10] = NA #column for PR-2
			aggColTemplate[11] = 3 #column for Irr
			aggColTemplate[12] = "Self" # IrrSrc Value
			aggColTemplate[13] = 4 #column for Tamb
			aggColTemplate[14] = 5 #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
		}
		else if(grepl("MFM",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 9 #column for LastRead
			aggColTemplate[4] = 10 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 5 #column for Yld-1
			aggColTemplate[8] = 6 #column for Yld-2
			aggColTemplate[9] = 7 #column for PR-1
			aggColTemplate[10] = 8 #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = "IN-036C-WMS" # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			aggColTemplate[16] = NA #column for IA
			aggColTemplate[17] = 11 #column for GA
			aggColTemplate[18] = 12 #column for PA
		}
		else if(grepl("Inverter",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 8 #column for LastRead
			aggColTemplate[4] = 9 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 6 #column for Yld-1
			aggColTemplate[8] = 7 #column for Yld-2
			aggColTemplate[9] = NA #column for PR-1
			aggColTemplate[10] = NA #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = NA # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			aggColTemplate[16] = 10 #column for IA
		}
		}
		registerColumnList("IN-036C",METERACNAMESAGG[x],aggNameTemplate,aggColTemplate)
}
