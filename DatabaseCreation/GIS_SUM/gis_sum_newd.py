import os
import re
import pyodbc
import csv,time
import datetime
import collections
import os
import time
import pytz


server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

tz = pytz.timezone('Asia/Kolkata')
read_path = '/home/admin/Dropbox/GIS/Summary/NewDelhi/NEWD Aggregate.txt' ###server

#Function to update at 12PM and 6PM
def determine_datetime():
    tz = pytz.timezone('Asia/Kolkata')
    now_time = datetime.datetime.now()
    curr_date = datetime.datetime.now().date()
    curr_time = curr_date.strftime("%Y-%m-%d %H:%M:%S")
    curr_time = datetime.datetime.strptime(curr_time, "%Y-%m-%d %H:%M:%S")
    curr_time12 = curr_time + datetime.timedelta(hours = 12)
    curr_time13 = curr_time + datetime.timedelta(hours = 13)
    curr_time18 = curr_time + datetime.timedelta(hours = 18)
    curr_time19 = curr_time + datetime.timedelta(hours = 19)
    if ((curr_time12 <= now_time <= curr_time13) or (curr_time18 <= now_time <= curr_time19)):
        print(now_time)
        result = 1
    else:
        result = 0
        
    return result

def insert(fields):
 
    counter = 0
    while True:            
        try:
            cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
            cursor = cnxn.cursor()

            global first_del
            if first_del == 1:
                first_del = 0
                with cursor.execute("TRUNCATE TABLE [Cleantech Meter Readings].dbo.[GIS_SUM_NEWD_AGG]"):
                    print('Detetion successful')
                    
            with cursor.execute("INSERT INTO [Cleantech Meter Readings].dbo.[GIS_SUM_NEWD_AGG] ([Date1],[GHI],[GTI]) VALUES (?,?,?)",fields): 
                print('Successfuly Inserted!' + fields[0])
                            
            
            cnxn.commit()
            cursor.close()
            cnxn.close()
            break
        
        except Exception as e:
            err_num = e[0]
            print ('ERROR:', err_num, fields[0])
            cursor.close()
            cnxn.close()
            if err_num == '08S01':
                counter = counter + 1
                print ('Sleeping for 5')
                time.sleep(300)
                if counter > 10:
                    exit('Persistent connection error')                    
                print ('%%%Re-trying connection%%%')             
            else:
                return

def convert_data_type(fields):
    for i in range(len(fields)):
        if i == 0:
            continue
        if fields[i] == 'NA' or fields[i] == 'NA\n' :
            fields[i] = None
        else:
            fields[i] = float(fields[i])

    return fields
        


while True:

    result = determine_datetime()
    if result ==1:
        try:
            if not os.path.exists(read_path):
                    raise Exception('Empty')        
        except Exception as e:
                print('No file found -',read_path)
                time.sleep(600)
                continue
            
        first_del = 1
        with open(read_path, 'r') as read_file:
            first = 1
            for line in read_file:
                if first == 1:
                    first = 0
                    continue
                fields =[]
                fields = line.split("\t")
                if fields[0] == "NA":
                    continue
                for i in range(len(fields)):   
                    fields[i] = fields[i].replace('"', '').strip()
                
                fields = convert_data_type(fields)
                insert(fields)
                
    time.sleep(3600)

