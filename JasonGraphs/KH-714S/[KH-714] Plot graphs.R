#source('/home/admin/Jason/cec intern/codes/711/[711] raw to second gen data.R')
#source('/home/admin/Jason/cec intern/codes/711/[711] Data_extract.R')
rm(list=ls(all =TRUE))

library(ggplot2)

pathWrite <- "/tmp/"
result <- read.csv("/tmp/[KH-714]_summary.csv")
dyap <- "/tmp/[714]doubleyaxisplot.pdf"
dyar <- "/tmp/[714]doubleyaxisratio.pdf"

#manually add in x axis labels for last 2 graphs (every 3 months)
monthslabel <- c("Jul/16","Oct/16","Jan/17","Apr/17",
                  "Jul/17","Oct/17","Jan/18","Apr/18",
                  "Jul/18","Oct/18", "Jan/19")

rownames(result) <- NULL
result <- data.frame(result)

result <- result[-length(result[,1]),]

result <- result[,-1]
colnames(result) <- c("date","da","pts","gsi","gmodE","gmodW","ratio1","ratio2")
result[,1] <- as.Date(result[,1], origin = result[,1][1])
result[,2] <- as.numeric(paste(result[,2]))
result[,3] <- as.numeric(paste(result[,3]))
result[,4] <- as.numeric(paste(result[,4]))
result[,5] <- as.numeric(paste(result[,5]))
result[,6] <- as.numeric(paste(result[,6]))
result[,7] <- as.numeric(paste(result[,7]))
result[,8] <- as.numeric(paste(result[,8]))


date <- result[,1]
last <- tail(result[,1],1)    #date[length(date)] works
first <- date[1]   #date[1]

result[,4] <- result[,4]/60000

dagraph <- ggplot(result, aes(x=date,y=da))+ylab("Data Availability [%]")
daFinalGraph<- dagraph + geom_line(size=0.5)+
  theme_bw()+
  expand_limits(x=date[1],y=0)+
  scale_y_continuous(breaks=seq(0, 115, 10))+
  scale_x_date(date_breaks = "3 month",date_labels = "%b/%y") +
  
  #scale_x_date(breaks=seq("2016-06-05","2016-09-18","3 month"),date_labels = "%b/%y")+
  #scale_y_continuous(limits = c(65, 86), expand= c(0,0), breaks=seq(70,90,10))

  ggtitle(paste("[KH-714S] Cambodia Met Station - Data Availability"), subtitle = paste0("From ",date[1]," to ",last))+
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
        plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5))+
  annotate("text",label = paste0("Average data availability = ", round(mean(result[,2]),1),"%"),size=3.7,
           x = as.Date(date[round(0.5*length(date))]), y= 113)+
  annotate("text",label = paste0("Lifetime = ", length(date)," days (",format(round(length(date)/365,1),nsmall = 1)," years)   "),size = 3.7,
           x = as.Date(date[round(0.5*length(date))]), y= 106)+
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

daFinalGraph #+ coord_fixed(ratio=3) #dont work with ggsave

#ggsave(daFinalGraph,filename = paste0(pathWrite,"KH-714_DA_LC.pdf"))
ggsave(daFinalGraph,filename = paste0(pathWrite,"KH-714_DA_LC.pdf"), width = 7.92, height = 5)


gsiGraph <- ggplot(result, aes(x=date,y=gsi))+ylab("GHI [kW/m2]")
gsiFinal_graph<- gsiGraph + geom_bar(stat = "identity", width = 1, position = "dodge")+
  theme_bw()+
  expand_limits(x=date[1],y=9)+
  #expand_limits(x=date[1],y=7.5)+
  scale_y_continuous(breaks=seq(0, 9, 1))+
  scale_x_date(date_breaks = "3 month",date_labels = "%b/%y")+
  ggtitle(paste("[KH-714S] Global Horizontal Irradiation Daily"), subtitle = paste0("From ",date[1]," to ",last))+
  theme(axis.title.y = element_text(face = "bold",size = 11,margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
        plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5))+
  geom_hline(yintercept=5.252, size=0.3, color ="blue")+
  annotate("text",label = paste0("Long-term average annual GHI = 1,917 kWh/m2"),size=3.7,
           x = as.Date(date[round(0.518*length(date))]), y= 9.0)+
  annotate("text",label = paste0("Long-term average daily GHI = 5.25 kWh/m2 day"),size=3.7,
           x = as.Date(date[round(0.518*length(date))]), y= 8.5, color="blue")+
  annotate("text",label = paste0("Current average daily GHI = ",format(round(mean(result[,4]),3),nsmall =3)," kWh/m2.day"),size=3.7,
           x = as.Date(date[round(0.518*length(date))]), y= 8.0, color="red")+
  geom_hline(yintercept=round(mean(result[,4]),2), size=0.3, color="red")+
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))

gsiFinal_graph

ggsave(gsiFinal_graph,filename = paste0(pathWrite,"KH-714_GSI_LC.pdf"), width = 7.92, height = 5)

tamb <- as.numeric(paste(result[,5]))
hamb <- as.numeric(paste(result[,6]))
tamb[1:2] <- NA 
hamb[1:2] <- NA

#pdf("C:/Users/talki/Desktop/CleanTech/result/weathermetric/711/711doubleyaxisplot1.pdf",width=7.92,height=5)
pdf(dyap, width =8, height=5)

yaxis <- tamb
yaxis2 <- hamb
tamb_mean <- mean(tamb,na.rm=TRUE)
hamb_mean <- mean(hamb,na.rm=TRUE)
xaxis <- c(1:length(yaxis))
xaxis2 <- seq(0,length(yaxis)+70,95)  # here whats need to change if labels length differ error

b_unit <- expression(~degree~C) 

par(mar=c(3.5, 4, 3, 4) + 0.1)

## Plot first set of data and draw its axis
plot(xaxis, tamb, pch=4, axes=FALSE, ylim=c(-100,75), xlab="", ylab="", 
     type="l",col="orange", main=" ")
axis(2, ylim=c(-100,75),at = seq(-100,75,25),col="black",col.axis="black",las=1)
mtext(expression(bold("Tamb [ C]")),side=2,line=2.5,cex=1,las =3)

box()

## Allow a second plot on the same graph
par(new=TRUE)

## Plot the second plot and put axis scale on right
plot(xaxis, hamb, pch=3,  xlab="", ylab="", ylim=c(-0,250), 
     axes=FALSE, type="l", col="blue")

axis(4, ylim=c(-0,250), col="black",col.axis="black",las=1)
text(length(xaxis)*1.15,125,expression(paste(bold("Hamb [%]"))),xpd=NA,srt = -90, cex = 1)

title(paste("[KH-714S] Daily Average Ambient Temperature and Relative Humidity \n From ", date[1], " to ", last), col="black", cex.main =1)

text(length(xaxis)/2, 225, paste("Average Tamb =",round(tamb_mean,1),"C"),cex = .8,col = "orange")
text(length(xaxis)/2, 105, paste("Average Hamb =",round(hamb_mean,1),"%"),cex = .8,col = "blue")

#axis(1, at = xaxis2, cex.axis = 0.53, labels = c("Jul/16","Oct/16","Jan/17","Apr/17","Jul/17","Oct/17","Jan/18","Apr/18","Jul/18","Oct/18"))
axis(1, at = xaxis2, cex.axis = 0.85, labels = monthslabel)

legend(length(yaxis)*0.85,260,
       c("Tamb","Hamb"),# places a legend at the appropriate place c(???Health???,???Defense???), # puts text in the legend
       lty=c(1,1), # gives the legend appropriate symbols (lines)
       lwd=c(2.5,2.5),col=c("orange","blue")) # gives the legend lines the correct color and width

par(new=TRUE)


dev.off()
ratio1 <- as.numeric(paste(result[,7]))
ratio2 <- as.numeric(paste(result[,8]))
ratio1 <- ratio1[-(1:2)]
ratio2 <- ratio2[-(1:2)]
ratio1[c(656,938)] <- NA
ratio2[c(13,43,159,461)] <- NA

pdf(dyar,width=8, height=5)
yaxis <- ratio1
yaxis2 <- ratio2
xaxis <- c(1:length(yaxis))
xaxis2 <- seq(0,length(yaxis)+70,95)


par(mar=c(3.5, 4, 3, 4) + 0.1)

## Plot first set of data and draw its axis
plot(xaxis, ratio1, pch=4, axes=FALSE, ylim=c(0,150), xlab="", ylab="", 
     type="l",col="blue", main=" ")
axis(2, ylim=c(0,150),at = seq(0,150,25),col="black",col.axis="black",las=1)
mtext(expression(bold("Pyr/Gsi02 Ratio [%]")),side=2,line=2.5,cex=1,las =3)
#text(-27,35,b_unit,xpd=NA,srt = 90, cex = 1.1)

box()

## Allow a second plot on the same graph
par(new=TRUE)

## Plot the second plot and put axis scale on right
plot(xaxis, ratio2, pch=3,  xlab="", ylab="", ylim=c(50,200), 
     axes=FALSE, type="l", col="orange")

axis(4, ylim=c(50,200), at = seq(-50,200,25),col="black",col.axis="black",las=1)
text(length(xaxis)*1.15,125,expression(paste(bold("Gsi01/Gsi02 Ratio [%]"))),xpd=NA,srt = -90, cex = 1)

title(paste("[KH-714S] Pyr/Gsi00 & GmodE/GmodW Ratio Plot \n From ", date[1], " to ", last), col="black", cex.main =1)

text(length(xaxis)/2, 180, paste("Average Pyr/Gsi00 ratio = +",format(round(mean(ratio1-100,na.rm=TRUE),1),nsmall =1),"%"),cex = .8,col="blue")
text(length(xaxis)/2, 60, paste("Average GmodE/GmodW ratio = +",format(round(mean(ratio2-100,na.rm=TRUE),1),nsmall = 1),"%"),cex = .8,col = "orange")

#axis(1, at = xaxis2, cex.axis = 0.53, labels = c("Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep","Oct","Nov","Dec","Jan","Feb",
#                                                "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec","Jan","Feb","Mar","Apr"))
axis(1, at = xaxis2, cex.axis = 0.85, labels = monthslabel)
legend(length(yaxis)*0.715,208,
       c("Pyr/Gsi00","GmodE/GmodW"),# puts text in the legend
       lty=c(1,1), # gives the legend appropriate symbols (lines)
       lwd=c(2.5,2.5),col=c("blue","orange")) # gives the legend lines the correct color and width

par(new=TRUE)

dev.off()

print(paste0("Graphs located @  ",pathWrite))
daFinalGraph
