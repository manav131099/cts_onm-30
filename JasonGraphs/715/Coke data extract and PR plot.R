rm(list=ls(all =TRUE))
library(ggplot2)
library(zoo)
library(lubridate)
# Using 3rd gen data; not available in Sharepoint
pathRead <- "/home/admin/Dropbox/Third Gen/[KH-003X]"
pathwritetxt <- "/home/admin/Jason/cec intern/results/715/[715]_summary_PR_w_filter.txt"
dspcoke <- '/home/admin/Jason/cec intern/results/715/[DSPCoke] PR.pdf'
dspsolar <- '/home/admin/Jason/cec intern/results/715/PR Progression - KH-003.pdf'
filelist <- dir(pathRead,pattern = ".txt", recursive= TRUE)

dff = NULL

for(z in filelist[1:length(filelist)])
{
	if(grepl("2016",z))
		next
  temp <- read.table(paste(pathRead,z,sep="/"), header = T, sep = "\t")
  df <- temp[,c(1,23,20,28,27)]
  dff= rbind(dff, df)

}

dff = as.data.frame(dff)
for(x in 1 :nrow(dff))
{
	if(is.finite(dff[x,4]) && (as.numeric(dff[x,4]) < 10))
		dff[x,4] = NA
}

dff = dff[complete.cases(as.numeric(dff[,3])),]
dff = dff[complete.cases(as.numeric(dff[,4])),]
dff = dff[complete.cases(as.numeric(dff[,5])),]
dff[,3] = dff[,3]/1000
firstdate <- as.Date(dff[1,1])
lastdate <- as.Date(dff[length(dff[,1]),1])
#moving average 30d
zoo.pr1 <- zoo(dff[,4], as.Date(dff$Date))
ma1 <- rollapplyr(zoo.pr1,list(-(29:1)), mean, fill = NA, na.rm = T)
dff$ambpr1.av=coredata(ma1)

zoo.pr2 <- zoo(dff[,5], as.Date(dff$Date))
ma2 <- rollapplyr(zoo.pr2,list(-(29:1)), mean, fill = NA, na.rm = T)
dff$ambpr2.av=coredata(ma2)

#data sorting for visualisation
#circle >  20
#triangle >=10 and <= 20
#square < 10
dff$shape[dff[,3] < 10] <- 'square'
dff$shape[dff[,3] >= 10 & dff[,3] <=20] <- 'triangle'
dff$shape[dff[,3] > 20 & dff[,3] <= 30] <- 'circle'
dff$shape[dff[,3] > 30] <- 'diamond'
print("-----------")
print(length(dff$shape[dff[,3] > 30]))
print("-----------")

dff$colour[dff[,2] < 2] <- '< 2'
dff$colour[dff[,2] >= 2 & dff[,2] <= 4] <- '2 ~ 4'
dff$colour[dff[,2] >= 4 & dff[,2] <= 6] <- '4 ~ 6'
dff$colour[dff[,2] > 6] <- '> 6'

dff$shape = factor(dff$shape, levels = c("square", "triangle", "circle","diamond"), labels = c("square", "triangle", "circle","diamond") )       #to fix legend arrangement

dff$colour = factor(dff$colour, levels = c("< 2", "2 ~ 4","4 ~ 6", "> 6"), labels =c('< 2','2 ~ 4','4 ~ 6','> 6') )       #to fix legend arrangement

titlesettings <- theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
                  plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5))

PRUse = as.numeric(dff$DSPCoke)
PRUse = PRUse[complete.cases(PRUse)]
last30 = round(mean(tail(PRUse,30)),1)
last60 = round(mean(tail(PRUse,60)),1)
last90 = round(mean(tail(PRUse,90)),1)
last365 = round(mean(tail(PRUse,365)),1)
lastlt = round(mean(PRUse),1)
p1 <- ggplot() + theme_bw()
rightIdx = nrow(dff) * 0.7
x1idx = round(nrow(dff) * .2,0)
x2idx = round(nrow(dff) * 0.15,0)
p1 <- p1 + geom_point(data=dff, aes(x=as.Date(Date), y =DSPCoke, shape = shape, colour = colour), size = 1.5)
p1 <- p1 +  ylab("Performance Ratio [%]") + xlab("") + coord_cartesian(ylim = c(0,100))
p1 <- p1 + scale_x_date(expand = c(0, 0),date_breaks = "3 months",date_labels = "%b/%y",limits = c(as.Date('2017/01/01'), NA)) + scale_y_continuous(breaks=seq(0, 100, 10))
p1 <- p1 + theme(axis.title.x = element_text(size=13), axis.title.y = element_text(size=13),
                 axis.text = element_text(size=13), panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black"),
                 legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97),axis.text.x = element_text(angle = 90,vjust = 0.5, hjust=1))  #LEGNED AT RHS
p1 <- p1 + titlesettings + ggtitle('Performance Ratio Progression - KH-003', subtitle = paste0('From ', firstdate, ' to ', lastdate))
p1 <- p1 + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))
p1 <- p1 + scale_colour_manual('Daily Irradiation [kWh/m2]', 
                               values = c("blue", "deepskyblue1", "orange","darkorange3"), 
                               labels = c('< 2','2 ~ 4','4 ~ 6','> 6'), guide = guide_legend(title.position = "left", ncol = 4,nrow=1)) 
p1 <- p1 + scale_shape_manual('Daily Loads [MWh]',
                              labels = c('< 10','10 ~ 20','20 ~ 30','> 30'),
                              values = c(15,17,19,18), guide = guide_legend(title.position = "left",ncol=4,nrow=1))
p1 <- p1 + theme(legend.box = "horizontal",legend.direction="horizontal") #legend.position = "bottom")


prbudget = 64.3
rate=0.008
coddate=as.Date('2016-10-06')
prthresh = c(prbudget,prbudget)
dates = c(firstdate,coddate)
temp=prbudget
date = dff[length(dff[,1]),1]
while(coddate<as.Date(date) && (as.Date(date)-coddate)>365){#
     coddate= coddate %m+% months(12)
     temp=temp-(temp*rate)
     prthresh=c(prthresh,temp)
     dates=c(dates,coddate)
}
prthresh=c(prthresh,temp)
dates=c(dates,lastdate)
p1 <- p1 +geom_step(mapping=aes(x=dates,y=prthresh),size=1,color='darkgreen')
dff$Date=as.Date(dff$Date, format = "%Y-%m-%d")
total=0
text="Target Budget Yield Performance Ratio ["
for(i in 2:(length(dates)-1)){#n-1 because we are taking number of years between. 
    if(i==(length(prthresh)-1)){
    text=paste(text,i-1,'Y-',format(round(prthresh[i],1),nsmall=1), "%]", sep="")
    }else{
    text=paste(text,i-1,'Y-',format(round(prthresh[i],1),nsmall=1), "%,", sep="")
    }
}
for(i in 1:(length(dates)-1)){
    if(i==(length(dates)-1)){
      total=total+sum(dff[dff$Date >= dates[i] & dff$Date <=dates[i+1],'DSPCoke']>prthresh[i])
    }else{
      total=total+sum(dff[dff$Date >= dates[i] & dff$Date < dates[i+1],'DSPCoke']>prthresh[i])
    }
    
}


p1 <- p1 + geom_line(data=dff, aes(x=as.Date(Date), y=ambpr1.av),color="red", size=1.5) 
p1 <- p1 + annotate("segment", x = as.Date(dff[nrow(dff)* 0.29,1]), xend = as.Date(dff[nrow(dff)* 0.33,1]), y=40, yend=40, colour="darkgreen", size=1.5)
p1 <- p1 + annotate('text', label = paste(text, sep=""), y = 40, x = as.Date(dff[round(nrow(dff) * 0.35, 0),1]), colour = "darkgreen",fontface =2,hjust = 0)
p1 <- p1 + annotate("segment", x = as.Date(dff[nrow(dff)* 0.29,1]), xend = as.Date(dff[nrow(dff)* 0.33,1]), y=35, yend=35, colour="red", size=1.5)
p1 <- p1 + annotate('text', label = "30-d moving average of PR", y = 35, x = as.Date(dff[round(nrow(dff) * 0.35, 0),1]), colour = "red",fontface =2,hjust = 0)

dff$Date=as.Date(dff$Date, format = "%Y-%m-%d")
p1 <- p1 + annotate('text', label = paste("Points above Target Budget PR = ",total,'/',nrow(dff),' = ',round((total/nrow(dff))*100,1),'%',sep=''), y = 30, x = as.Date(dff[round(nrow(dff) * 0.35, 0),1]), colour = "black",fontface =2,hjust = 0)

p1 <- p1 + annotate('text', label = paste("Average PR last 30-d:",last30,"%"), y = 20 , x = as.Date(dff[rightIdx,1]), colour = "black", hjust = 0)
p1 <- p1 + annotate('text', label = paste("Average PR last 60-d:",last60,"%"), y = 15 , x = as.Date(dff[rightIdx,1]), colour = "black", hjust = 0)
p1 <- p1 + annotate('text', label = paste("Average PR last 90-d:",last90,"%"), y = 10 , x = as.Date(dff[rightIdx,1]), colour = "black", hjust = 0)
p1 <- p1 + annotate('text', label = paste("Average PR last 365-d:",last365,"%"), y = 5 , x = as.Date(dff[rightIdx,1]), colour = "black", hjust = 0)
p1 <- p1 + annotate('text', label = paste("Average PR Lifetime:", lastlt,"%"), y=0, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0,  fontface =2)
p1 <- p1 + annotate("segment", x = as.Date(dff[nrow(dff),1]), xend = as.Date(dff[nrow(dff),1]), y = 0, yend = 100 , colour = "deeppink4", size=1, alpha=0.5)


## second pr
#3 month %b %d %y
meanDSPSol = round(mean(as.numeric(dff$DSPSolar)),1)
p2 <- ggplot() + theme_bw()
p2 <- p2 + geom_point(data=dff, aes(x=as.Date(Date), y =DSPSolar, shape = shape, colour = colour), size = 1.5)
p2 <- p2 +  ylab("Performance Ratio [%]") + xlab(" ") + coord_cartesian(ylim = c(0,100))
p2 <- p2 + scale_x_date(date_breaks = "3 month",date_labels = "%b %y") + scale_y_continuous(breaks=seq(0, 100, 10))
p2 <- p2 + theme(axis.title.x = element_text(size=19), axis.title.y = element_text(size=19),
                 axis.text.y = element_text(size=13), axis.text.x=element_text(size=8),panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black"),
                 legend.justification = c(1, 1), legend.position = c(1, 1))                     #LEGNED AT RHS
p2 <- p2 + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))
p2 <- p2 + titlesettings + ggtitle('PR Progression - KH-003', subtitle = paste0('From ', firstdate, ' to ', lastdate))
p2 <- p2 + scale_colour_manual('Irradiation [kWh/m2]', 
                               values = c("blue", "deepskyblue1","orange","darkorange3"), 
                               labels = c('< 2','2 ~ 4','4 ~ 6','> 6'), guide = guide_legend(title.position = "top", nrow = 1)) 
p2 <- p2 + scale_shape_manual('Loads [MWh]',
                              labels = c('< 10','10 ~ 20','20 - 30','> 30'),
                              values = c(15,17,19,18), guide = guide_legend(title.position = "top"))
p2 <- p2 + theme(legend.box = "horizontal")
p2 <- p2 + guides(colour = guide_legend(override.aes = list(shape = 18)))
p2 <- p2 + geom_hline(yintercept = 72, colour = "green", size = 1.1) + geom_hline(yintercept = meanDSPSol, colour = "black", size = 1.1)
#p2 <- p2 + geom_hline(yintercept = 60, colour = "green1", size= 1.1) + geom_hline(yintercept = 50, colour = "darkorange2", size =1.1) + geom_hline(yintercept = 40, colour = "gold1", size= 1.1)
p2 <- p2 + geom_line(data=dff, aes(x=as.Date(Date), y=ambpr2.av),color="red", size=1.5) 
p2 <- p2 + annotate('text', label = "30-d moving average of PR", y = 26 , x = as.Date(dff[x1idx,1]), colour = "red")
p2 <- p2 + annotate('text', label = "Target Budget PR", y = 75 , x = as.Date(dff[x1idx,1]), colour = "green")
p2 <- p2 + annotate('text', label = "Average PR", y = (meanDSPSol+3) , x = as.Date(dff[x1idx,1]), colour = "black")
p2 <- p2 + annotate("segment", x = as.Date(dff[x2idx,1]), xend = as.Date(dff[x2idx,1]), y = 28, yend = as.numeric(dff$ambpr2.av[x2idx]) , colour = "red", size=1.5, alpha=0.5, arrow=arrow())




ggsave(paste0(dspcoke), p1, width = 11, height=8)
ggsave(paste0(dspsolar), p2, width = 11, height=8)

write.table(dff,na = "",pathwritetxt,row.names = FALSE,sep ="\t")



