# -*- coding: utf-8 -*-
"""
Created on Fri Jul  3 14:38:56 2020

@author: saipr
"""

import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt
import matplotlib.dates as mdates

path=r'C:\Users\saipr\Desktop\Backup\Extracts\TH-007-MinuteAll-2019-06.txt'
df_minute=pd.read_csv(path,sep='\t')
fig = plt.figure(num=None, figsize=(20, 10))
ax = fig.add_subplot(111)
a=[]
b=[]
c=[]
for i in range(1,30):
    a.append((df_minute.loc[(df_minute['POAI_avg'] < 50.0*i) &  (df_minute['POAI_avg'] > 50.0*(i-1))  &  (df_minute['POAI_avg'] > 5),'POAI_avg'].count()/len(df_minute))*100)
    b.append(str(50*(i-1))+'-'+str(50*i))
    c.append((i)*.6)

greater_1000=round(((df_minute.loc[(df_minute['POAI_avg'] >1000),'POAI_avg'].count()/len(df_minute))*100),1)
tot_points=df_minute.loc[(df_minute['POAI_avg'] >5),'POAI_avg'].count()
plt.bar(c, a, width=0.5,color='red',zorder=2)
plt.xticks(c, b,rotation=90)
plt.xlim([0.2,15.9])
for i in range(1,8):
    plt.axhline(y=i, color='lightgrey',linewidth=1,zorder=1)
ax.set_ylabel('Frequency Distribution [%]', fontsize=15)
ax.set_xlabel('Global Horizontal Irradiance [W/m$^2$]', fontsize=15)
plt.show()
plt.axvline(x=12.3, color='black',linewidth=1.5,ls='dashed')
plt.text(13,4.1,'Irradiance occurrences \n> 1,000 W/m$^2$ = '+str(greater_1000)+'%',fontsize=11)
plt.text(8,4.1,'No of points > 5 W/m$^2$ = '+str(tot_points),fontsize=11)

fig.savefig(r"C:\Users\saipr\Desktop\[TH-007] Frequency Distribution-June.pdf", bbox_inches='tight')


fig2 = plt.figure(num=None, figsize=(20, 10))
ax2 = fig2.add_subplot(111)
a=[]
b=[]
c=[]
for i in range(1,30):
    a.append((df_minute.loc[(df_minute['POAI_avg'] < 50.0*i) &  (df_minute['POAI_avg'] > 50.0*(i-1))  &  (df_minute['POAI_avg'] > 5),'POAI_avg'].sum()/12000))
    b.append(str(50*(i-1))+'-'+str(50*i))
    c.append((i)*.6)
total=sum(a)

greater_1000=round(((df_minute.loc[(df_minute['POAI_avg'] >1000),'POAI_avg'].sum()/12000)/total)*100,1)
plt.bar(c, a, width=0.5,color='red',zorder=2)
plt.xticks(c, b,rotation=90)
plt.xlim([0.2,15.9])
ax2.set_ylabel('Irradiation Distribution [kWh/m$^2$]', fontsize=15)
ax2.set_xlabel('Global Horizontal Irradiance [W/m$^2$]', fontsize=15)
for i in range(1,11):
    plt.axhline(y=2.5*i, color='lightgrey',linewidth=1,zorder=1)
plt.show()
plt.axvline(x=12.3, color='black',linewidth=1.5,ls='dashed')
plt.text(.8,12,'Only Irradiance > 5 W/m$^2$ used ',fontsize=11)
plt.text(.8,10,'Total Irradiation = '+str(round(sum(a),1))+' [kWh/m$^2$]',fontsize=11)
plt.text(.8,14,'No of points > 5 W/m$^2$ = '+str(tot_points),fontsize=11)
plt.text(13,10,'Irradiation using values \n> 1,000 W/m$^2$ = '+str(greater_1000)+'%',fontsize=11)
print(len(df_minute.loc[(df_minute['POAI_avg'] < 100.0) & (df_minute['POAI_avg'] > 50.0),'POAI_avg'])/len(df_minute))
plt.ylim([0,20])
fig2.savefig(r"C:\Users\saipr\Desktop\[TH-007] Irradiation Distribution-June.pdf", bbox_inches='tight')
