rm(list = ls())
TIMESTAMPSALARM = NULL
TIMESTAMPSALARM2 = NULL
TIMESTAMPSALARM3 = NULL
TIMESTAMPSALARM4 = NULL
METERCALLED = 0
ltcutoff = .0001
CABLOSSTOPRINT = 0
INSTCAP=c(994.5)
TOTSOLAR = 0
source('/home/admin/CODE/TH002Digest/Invoicing.R')
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

getGTIData = function(day)
{
	yr = substr(day,1,4)
	yrmon = substr(day,1,7)
	path = paste("/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[TH-001C]/",yr,"/",yrmon,"/WMS/[TH-001C]-WMS-",day,".txt",sep="")
	GTI = Tamb=Tmod=NA
	if(file.exists(path))
	{
		dataread = read.table(path,header = T,sep="\t")
		if(nrow(dataread))
		{
			GTI = as.numeric(dataread[1,3])
			Tamb = as.numeric(dataread[1,4])
			Tmod = as.numeric(dataread[1,5])
		}
	}
	return(c(GTI,Tamb,Tmod))
}

patchLoadData = function(f1,f2,f3)
{
	d1 = read.table(f1,header=T,sep="\t",stringsAsFactors=F)
	d2 = read.table(f2,header=T,sep="\t",stringsAsFactors=F)
	d3 = read.table(f3,header=T,sep="\t",stringsAsFactors=F)
	
	t1 = t2 = unlist(rep(NA,nrow(d3)))

	d3time = as.character(d3[,1])
	d2time = as.character(d2[,1])
	d1time = as.character(d1[,1]) 

	idxmtch = match(d2time, d3time)
	idxmtch2 = match(d3time, d2time)

	idxmtch = idxmtch[complete.cases(idxmtch)]
	idxmtch2 = idxmtch2[complete.cases(idxmtch2)]

	if(length(idxmtch) && length(idxmtch2))
	{
		t1[idxmtch] = as.numeric(d3[idxmtch,11]) + as.numeric(d2[idxmtch2,19])
	}
	
	idxmtch = match(d1time, d3time)
	idxmtch2 = match(d3time, d1time)

	idxmtch = idxmtch[complete.cases(idxmtch)]
	idxmtch2 = idxmtch2[complete.cases(idxmtch2)]

	if(length(idxmtch) && length(idxmtch2))
	{
		t2[idxmtch] = as.numeric(d3[idxmtch,11]) + as.numeric(d1[idxmtch2,19])
	}
	
	clnam = colnames(d3)
	clnam[69] = "Artificial_Load_Invoice"
	clnam[70] = "Artificial_Load_Check"
	d3[,69] = t1
	d3[,70] = t2
	
	colnames(d3) = clnam

	write.table(d3, file=f3,row.names=F,col.names=T,sep="\t",append=F)
}

secondGenData = function(filepath,writefilepath)
{
  {
		if(METERCALLED == 1){
	 		TIMESTAMPSALARM <<- NULL
			}
		else if(METERCALLED == 2){
			TIMESTAMPSALARM2 <<- NULL}
		else if(METERCALLED == 3)
		{
			TIMESTAMPALARM3 <<- NULL
		}
	}
	print(paste('IN 2G',filepath))
	date = Eac1 = DA = totrowsmissing = Yld1  = Eac2 = RATIO = ArtLoadInv = ArtLoadCheck=NA 
	lasttime = lastread = Yld2 = Gmod = Tamb =GPy=PRPy= ExpAmt=ExpPerc=PR1=PR2=Eac1N=NA
  dataread = try(read.table(filepath,header = T,sep = "\t",stringsAsFactors=F),silent=T)
	if(class(dataread) == "try-error")
	{
		
		{
		if(METERCALLED == 3)
		{
      df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),
									DA = DA,Downtime = totrowsmissing,
									Yld1=Yld1,ArtLoadInv = ArtLoadInv, ArtLoadCheck=ArtLoadCheck,
									EacFlowBack=Eac1N,
									stringsAsFactors = F)
		
		}
		else if (METERCALLED == 1)
		{
			df = data.frame(Date = date, LoadEac = as.numeric(Eac1),LoadEac2 = as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,Yld1=Yld1,RatioEac=RATIO,
									lasttime=lasttime,lastread=lastread,Yld2=Yld2,
									stringsAsFactors = F)
		}
		else if (METERCALLED == 2)
		{
				df = data.frame(Date = date, SolEac = as.numeric(Eac1),SolEac2= as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,Yld1=Yld1,RatioEac=RATIO,
									lasttime=lasttime,lastread=lastread,Gmod=Gmod,PR1=PR1,Tamb=Tamb,Yld2=Yld2,PR2=PR2,
#									GPy=GPy,PRPy=PRPy,
									stringsAsFactors = F)
		
		}
		}
		return()
	}

	dataread2 = dataread
	if(METERCALLED == 2)
		colnoUse = 46
	if(METERCALLED == 1)
		colnoUse = 46
	{
	if(METERCALLED == 1 || METERCALLED == 2)
	{
	idxpac = 19
	dataread = dataread2[complete.cases(as.numeric(dataread2[,colnoUse])),]
	lasttime=lastread=Eac2=NA
	if(nrow(dataread)>0)
	{
	lasttime = as.character(dataread[nrow(dataread),1])
	lastread = as.character(round(as.numeric(dataread[nrow(dataread),colnoUse])/1000,3))
  Eac2 = format(round((as.numeric(dataread[nrow(dataread),colnoUse]) - as.numeric(dataread[1,colnoUse]))/1000,2),nsmall=1)
	}
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	Eac1 = RATIO=NA
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(round(sum(as.numeric(dataread[,idxpac]))/60000,1),nsmall=2)
		RATIO = round(as.numeric(Eac1)*100/as.numeric(Eac2),1)
	}
	}
	else
	{
	idxpac = 11
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	Eac1 = NA
	if(nrow(dataread) > 1)
   	Eac1 = format(round(sum(as.numeric(dataread[,idxpac]))/60000,1),nsmall=2)
	dataread = dataread[as.numeric(dataread[,idxpac]) < 0,]
	Eac1N = NA
	if(nrow(dataread) > 1)
   	Eac1N = format(round(abs(sum(as.numeric(dataread[,idxpac])))/60000,1),nsmall=2)
	ArtLoadInv2 = as.numeric(dataread2[,69])
	ArtLoadCheck2 = as.numeric(dataread2[,70])
	ArtLoadInv2 = ArtLoadInv2[complete.cases(ArtLoadInv2)]
	ArtLoadCheck2 = ArtLoadCheck2[complete.cases(ArtLoadCheck2)]
	if(length(ArtLoadInv2))
		ArtLoadInv = format(round(sum(ArtLoadInv2)/60000,2),nsmall=2)
	if(length(ArtLoadCheck2))
		ArtLoadCheck = format(round(sum(ArtLoadCheck2)/60000,2),nsmall=2)
	}
	}
	dataread = dataread2
	datareadcp = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 480,]
  tdx = tdx[tdx > 480]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
  missingfactor = 540 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,idxpac]) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
		{
			if(METERCALLED == 1){
				TIMESTAMPSALARM <<- as.character(dataread2[,1])}
			else if(METERCALLED == 2){
		 	 TIMESTAMPSALARM2 <<- as.character(dataread2[,1])}
			else if(METERCALLED == 3){
		 	 TIMESTAMPSALARM3 <<- as.character(dataread2[,1])}
		}
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/5.4,1),nsmall=1)
	date = NA
	if(nrow(dataread)>0)
		date = substr(as.character(dataread[1,1]),1,10)
		indextouse = 1
	{
	  if(METERCALLED == 1 || METERCALLED == 2)
	  {
			Yld1 = format(round(as.numeric(Eac1)/INSTCAP[indextouse],2),nsmall=2)
			Yld2 = format(round(as.numeric(Eac2)/INSTCAP[indextouse],2),nsmall=2)

			{
			if(METERCALLED == 1)
      {
			df = data.frame(Date = date, LoadEac = as.numeric(Eac1),LoadEac2 = as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,Yld1=Yld1,RatioEac=RATIO,
									lasttime=lasttime,lastread=lastread,Yld2=Yld2,stringsAsFactors = F)
			}
			else
			{
				dataread2 = datareadcp
				dataTemp = as.numeric(dataread2[,(length(colnames(dataread2))-1)])
				dataTemp = dataTemp[complete.cases(dataTemp)]
				Gmod = Tamb = NA
				if(length(dataTemp))
					Gmod = round(sum(dataTemp)/60000,2)
				PR1 = round((as.numeric(Yld1) * 100 / Gmod),1)
				PR2 = round((as.numeric(Yld2) * 100 / Gmod),1)
				dataTemp = as.numeric(dataread2[,(length(colnames(dataread2)))])
				dataTemp = dataTemp[complete.cases(dataTemp)]
				if(length(dataTemp))
					Tamb = round(mean(dataTemp),1)
				TOTSOLAR <<- as.numeric(Eac2)
				df = data.frame(Date = date, SolEac = as.numeric(Eac1),SolEac2= as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,Yld1=Yld1,RatioEac=RATIO,
									lasttime=lasttime,lastread=lastread,Gmod=Gmod,PR1=PR1,Tamb=Tamb,Yld2=Yld2,PR2=PR2,
#									GPy=GPy,PRPy=PRPy,
									stringsAsFactors = F)
			}
			}
    } 
	  else if(METERCALLED == 3)
	  {
			Yld1 = format(round(as.numeric(Eac1)/INSTCAP[indextouse],2),nsmall=2)
      df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),
									DA = DA,Downtime = totrowsmissing,
									Yld1=Yld1,ArtLoadInv = ArtLoadInv, ArtLoadCheck=ArtLoadCheck,EacFlowBack=Eac1N,
									stringsAsFactors = F)
		}
	}
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1, filepathm2,filepathm3,writefilepath,invoicingData)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F) #its correct dont change
  dataread2 = read.table(filepathm2,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dataread3 = read.table(filepathm3,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	
	dt = as.character(dataread2[,5])
	da = as.character(dataread2[,4])
	EacLoad1 = as.numeric(dataread3[,2])
	
	ArtLoadInv = as.numeric(dataread3[,6])
	ArtLoadChk = as.numeric(dataread3[,7])

	EacSol1Meth1 = as.numeric(dataread2[,2])
	EacSol1Meth2 = as.numeric(dataread2[,3])
	RatioSol1 = as.numeric(dataread2[,7])
	EacLoadMeth1 = as.numeric(dataread1[,2])
	EacLoadMeth2 = as.numeric(dataread1[,3])
	RatioLoad = as.numeric(dataread1[,7])
	LastReadSol = as.numeric(dataread2[,8])
	LastTimeSol = as.numeric(dataread2[,9])
	LastTimeLoad = as.numeric(dataread1[,8])
	LastReadLoad = as.numeric(dataread1[,9])
	GsiTot = as.numeric(dataread2[,10])
	PR1 = as.numeric(dataread2[,11])
	PR2 = as.numeric(dataread2[,14])
	YldSol = as.numeric(dataread2[,6])
	YldSol2 = as.numeric(dataread2[,12])
	YldLoad = as.numeric(dataread1[,6])
	YldLoad2 = as.numeric(dataread1[,10])
	Tamb = as.numeric(dataread2[,11])
	Eac1N = as.numeric(dataread3[,8])
	PercExp = round((Eac1N * 100/EacSol1Meth2),1)
	
	df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),
	DA= da,
	Downtime = dt,
	EacLoadCom = EacLoad1,
	YldLoadComp = YldLoad2,
	EacSolMeth1=EacSol1Meth1,
	EacSolMeth2=EacSol1Meth2,
	RatioSol=RatioSol1,
	LastTimeSol = LastTimeSol,
	LastReadSol = LastReadSol,
	GsiTot = GsiTot,
	PR1 = PR1,
	YldSol = YldSol,
	EacLoadMeth1=EacLoadMeth1,
	EacLoadMeth2=EacLoadMeth2,
	RatioLoad=RatioLoad,
	LastTimeLoad = LastTimeLoad,
	LastReadLoad = LastReadLoad,
	YldLoad = YldLoad,
	Tamb = Tamb,
	PeakEac=as.numeric(invoicingData[,8]),
	OffPeakEac=as.numeric(invoicingData[,9]),
	PeakEacPerc = as.numeric(invoicingData[,10]),
	OffPeakEacPerc = as.numeric(invoicingData[,11]),
	Day = as.character(invoicingData[,2]),
	Holiday=as.character(invoicingData[,3]),
	PR2=PR2,
	ArtLoadInv = ArtLoadInv,
	ArtLoadCheck = ArtLoadChk,
	EacFlowBack = Eac1N,
	PercExp = PercExp,
	stringsAsFactors=F)

  {
    if(file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}

