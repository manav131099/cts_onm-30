errHandle = file('/home/admin/Logs/LogsVN001Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/VN001XDigest/HistoricalAnalysis2G3GVN001.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/VN001XDigest/aggregateInfo.R')
initDigest = function(df)
{
 body = "\n\n_________________________________________\n\n"
  body = paste(body,as.character(df[,1]))
  body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"DA [%]: ",as.character(df[,2]),sep="")
  body = paste(body,"\n\nDowntime [%]: ",as.character(df[,3]),sep="")
  body = paste(body,"\n\nEac-1 Inverter-1 [kWh]: ",format(round(as.numeric(df[,4])),nsmall=1))
  body = paste(body,"\n\nEac-1 Inverter-2 [kWh]: ",format(round(as.numeric(df[,5])),nsmall=1))
  body = paste(body,"\n\nEac-1 Inverter-3 [kWh]: ",format(round(as.numeric(df[,6])),nsmall=1))
  body = paste(body,"\n\nEac-1 Inverter-4 [kWh]: ",format(round(as.numeric(df[,7])),nsmall=1))
  body = paste(body,"\n\nEac-1 Inverter-5 [kWh]: ",format(round(as.numeric(df[,8])),nsmall=1))
  body = paste(body,"\n\nEac-1 Inverter-6 [kWh]: ",format(round(as.numeric(df[,9])),nsmall=1))
  body = paste(body,"\n\nEac-1 Solar Plant [kWh]: ",format(round(as.numeric(df[,10])),nsmall=1))
  body = paste(body,"\n\nYield-1 Inverter-1 [kWh/kWp]: ",as.character(df[,16]),sep="")
  body = paste(body,"\n\nYield-1 Inverter-2 [kWh/kWp]: ",as.character(df[,17]),sep="")
  body = paste(body,"\n\nYield-1 Inverter-3 [kWh/kWp]: ",as.character(df[,18]),sep="")
  body = paste(body,"\n\nYield-1 Inverter-4 [kWh/kWp]: ",as.character(df[,19]),sep="")
  body = paste(body,"\n\nYield-1 Inverter-5 [kWh/kWp]: ",as.character(df[,20]),sep="")
  body = paste(body,"\n\nYield-1 Inverter-6 [kWh/kWp]: ",as.character(df[,21]),sep="")
  body = paste(body,"\n\nYield-1 Solar Plant [kWh/kWp]: ",as.character(df[,13]),sep="")
  body = paste(body,"\n\nStdev / COV Inverter yields: ",as.character(df[,22])," / ",as.character(df[,23]),"%",sep="")
  body = paste(body,"\n\nIrradiance (from VN-001S) [kWh/m2]: ",as.character(df[,14]),sep="")
  body = paste(body,"\n\nPR [%]: ",as.character(df[,15]),sep="")
  body = paste(body,"\n\nEac-1 Grid [kWh]: ",as.character(df[,11]),sep="")
  body = paste(body,"\n\nSolar Penetration [%]: ",as.character(df[,12]),sep="")
  return(body)
}

printtsfaults = function(TS,body)
{
	if(length(TS) > 1)
	{
		body = paste(body,"\n\n_________________________________________\n\n")
		body = paste(body,paste("Timestamps where Pac < 1 between 8am -5pm\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
	}
	return(body)
}

sendMail = function(df1,pth1,pth5)
{
  filetosendpath = c(pth1,pth5)
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	print('Filenames Processed')
	body=""
	
	body = paste(body,"Site Name: Schneider Electric Vietnam\n",sep="")
	body = paste(body,"\nLocation: Ho Chi Minh City, Vietnam\n")
	body = paste(body,"\nO&M Code: VN-001\n")
	body = paste(body,"\nSystem Size: 422.5 kWp\n")
	body = paste(body,"\nNumber of Energy Meters: 1\n")
	body = paste(body,"\nModule Brand / Model / Nos: Jinko Solar / JKM325PP-72 / 1300\n")
	body = paste(body,"\nInverter Brand / Model / Nos: Schneider Electric / Conext CL-60E / 6\n")
	body = paste(body,"\nSite COD:",DOB)
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(DAYSALIVE))))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(DAYSALIVE))/365,1)))
  
	body = paste(body,initDigest(df1))  #its correct, dont change
  body = printtsfaults(TIMESTAMPSALARM,body)
	print('2G data processed')
	body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"Station Data")
  body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"Station DOB:",as.character(DOB))
  body = paste(body,"\n\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive:",yrsalive)
	print('3G data processed')
	body = gsub("\n ","\n",body)
  graph_command=paste("python3 /home/admin/CODE/VN001XDigest/Inverter_CoV_Graph.py",'VN-001',substr(currday,11,nchar(currday)-4),sep=" ")
  print(graph_command)
	system(graph_command,wait=TRUE)
	print('Graph Done!')
	graph_path=paste('/home/admin/Graphs/Graph_Output/VN-001/[VN-001] Graph ',substr(currday,11,nchar(currday)-4),' - Inverter CoV.pdf',sep="")
  print(graph_path)
	graph_extract_path=paste('/home/admin/Graphs/Graph_Extract/VN-001/[VN-001] Graph ',substr(currday,11,nchar(currday)-4),' - Inverter CoV.txt',sep="")
  print(graph_extract_path)
  pathall = c(graph_path,graph_extract_path,filetosendpath)
	while(1)
	{
  mailSuccess = try(send.mail(from = sender,
            to = recipients,
            subject = paste("Station [VN-001X] Digest",substr(currday,11,20)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = pathall,
            #file.names = filenams, # optional parameter
            debug = F),silent=T)
	if(class(mailSuccess)=='try-error')
	{
		Sys.sleep(180)
		next
	}
	break
	}
	print(paste("currday substr is",substr(currday,11,20)))
	recordTimeMaster("VN-001X","Mail",substr(currday,11,20))
}

sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
# recipients = getRecipients("VN-001X","m")
recipients = c('operationsVN@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','diem-nh@nteco.com.vn','viet-nt@nteco.com.vn')


pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	# recipients = getRecipients("VN-001X","m")
  recipients = c('operationsVN@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','diem-nh@nteco.com.vn','viet-nt@nteco.com.vn')
  recordTimeMaster("VN-001X","Bot")
	sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathmonths = paste(pathyear,months[y],sep="/")
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[VN-001X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
      pathdays = pathmonths
      writepath2Gdays = writepath2Gmon
      checkdir(writepath2Gdays)
      days = dir(pathdays)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
           Sys.sleep(3600)
           next
         }
				 if(grepl("Copy",c(days[t])))
				 {
				 	daycop = c(days[t])
					daycop = daycop[grepl("Copy",daycop)]
					for(inner in 1 : length(daycop))
					{
				 	print('Copy file found so removing it')
					command = paste('rm "',pathdays,'/',daycop[t],'"',sep="")
					print(command)
					system(command)
					print('Command complete.. sleeping')
					}
				 	Sys.sleep(3600)
					next
				 }

				 if(is.na(days[t]))
				 {
					 	print('Files all NA restarting after hour')
           Sys.sleep(3600)
           next
				 }
				 todisp = 1
         sendmail = 1
         DAYSALIVE = DAYSALIVE + 1
				 print(paste('Processing',days[t]))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         readpath = paste(pathdays,days[t],sep="/")
         df1 = secondGenData(readpath,writepath2Gfinal)
				thirdGenData(writepath2Gfinal,writepath3Gfinal)
  if(sendmail ==0)
  {
    next
  }
	print('Sending Mail')
  sendMail(df1,writepath2Gfinal,writepath3Gfinal)
	print('Mail Sent')
      }
    }
  }
	gc()
}
sink()
